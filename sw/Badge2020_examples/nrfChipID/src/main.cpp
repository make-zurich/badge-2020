#include <Arduino.h>
/*
  Reads the Chip ID of an nRF52  based Arduino

  https://devzone.nordicsemi.com/f/nordic-q-a/46428/nrf-52832-chip-id-or-unique-serial-number
*/

// used for the array thing
using namespace std;

#include <Adafruit_NeoPixel.h>

#define LED_PIN 4

// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 11

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

array<uint32_t, 8> colors;

array<uint32_t, 8> getCols() {
  array<uint32_t, 8> ret;
  uint32_t adr = NRF_FICR->DEVICEADDR[1];
  ret[0] = ((adr >> 24) & 0xFF);
  ret[1] = ((adr >> 16) & 0xFF);
  ret[2] = ((adr >> 8) & 0xFF);
  ret[3] = (adr & 0xFF);
  adr = NRF_FICR->DEVICEADDR[0];
  ret[4] = ((adr >> 24) & 0xFF);
  ret[5] = ((adr >> 16) & 0xFF);
  ret[6] = ((adr >> 8) & 0xFF);
  ret[7] = (adr & 0xFF);
  Serial.printf("%02X %02X %02X %02X %02X %02X %02X %02X\n", ret[0], ret[1], ret[2], ret[3], ret[4], ret[5], ret[6], ret[7]);
  return ret;
}

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600);

  // prints title with ending line break
  Serial.printf("DevAddr %08X %08X\n", NRF_FICR->DEVICEADDR[1], NRF_FICR->DEVICEADDR[0] );
  colors = getCols();
  for (int i = 0; i < 8; i++) {
    int col = map(colors[i],0,0xFF,0,0x10000);
    strip.setPixelColor(i * 2, strip.gamma32(strip.ColorHSV(col)));
    strip.show();
    delay(150);
  }
}



void loop() {

}